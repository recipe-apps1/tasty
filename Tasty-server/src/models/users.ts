require('dotenv').config();
import mongoose, { Schema } from 'mongoose';
import { UserModelInterface, UserDocInterface, User } from '../types/types';
import bcrypt from 'bcrypt';
import jwt, { VerifyErrors } from 'jsonwebtoken';
const SALT = 10;

const usersSchema = new Schema(
  {
    name: {
      type: Schema.Types.String,
      required: [true, 'The name field is required'],
      minlength: 2,
      maxlength: 100
    },
    email: {
      type: Schema.Types.String,
      required: [true, 'The email field is required'],
      trim: true,
      unique: true,
      minlength: 3,
      maxlength: 320
    },
    password: {
      type: Schema.Types.String,
      required: [true, 'The password field is required'],
      minlength: 6
    },
    recipeList: {
        type: Schema.Types.Array,
        optional: true,
    },
    myRecipes: {
        type: Schema.Types.Array,
        optional: true,
    },
    authToken: {
      type: Schema.Types.String,
      optional: true
    }
  },
  {
    timestamps: true,
  },
);

//saving user data
usersSchema.pre('save', function (next) {
    const user = this;
    if (user.isModified('password')) { // checking if password field is available and modified
        bcrypt.genSalt(SALT, function (err, salt) {
            if (err) {
                return next(err)
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

//for generating token when loggedin
usersSchema.methods.generateToken = function (cb: Function) {
  const user = this;
  const JWT_SECRET = process.env.JWT_SECRET || "";
  const authToken = jwt.sign({ _id: user._id.toHexString() }, JWT_SECRET, { expiresIn: 1000000 });
  user.authToken = authToken;
  user.save((err: any, user: User) => {
    if (err) {
      return cb(err);
    }
    cb(null, user);
  });
};

//for comparing the users entered password with database duing login 
usersSchema.methods.comparePassword = function (candidatePassword: string, callBack: Function) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return callBack(err);
    callBack(null, isMatch);
  });
}

//validating token for auth routes middleware
usersSchema.statics.findByToken = function (authToken: string, callBack: Function) {
  const user = this;
  const JWT_SECRET = process.env.JWT_SECRET || "";
  jwt.verify(authToken, JWT_SECRET, {}, function (err: VerifyErrors | null, decode: any) {
      // if token is not valid return the error
      if (err) {
          return callBack(err);
      }
      //this decode must give user_id if token is valid .ie decode=user_id
      if (decode) {
        user.findOne({ "_id": decode._id, "authToken": authToken }, function (err: any, user: UserDocInterface) {
            if (err) {
                return callBack(err);
            }
            callBack(null, user);
        });
      }
  });
};

const Users = mongoose.model<UserDocInterface, UserModelInterface>('Users', usersSchema);

export default Users;