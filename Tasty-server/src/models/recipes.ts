import mongoose, { Schema } from 'mongoose';
import { RecipeModelInterface } from '../types/types';

const recipeSchema = new Schema(
  {
    title: {
        type: Schema.Types.String,
        required: [true, 'The title field is required'],
        maxlength: 100,
        minlength: 3
    },
    ingredients: {
        type: Schema.Types.Array,
        required: [true, 'The ingredients field is required']
    },
    tags: {
        type: Schema.Types.Array,
        required: [true, 'The tags field is required']
    },
    instructionLink: {
        type: Schema.Types.String,
        optional: true,
    },
    instructions: {
        type: Schema.Types.String,
        minlength: 20,
        optional: true,
    },
    image: {
        type: Schema.Types.String,
        required: [true, "An image for the recipe is required"]
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        required: true
    }
  },
  {
    timestamps: true,
  },
);

const Recipes = mongoose.model<RecipeModelInterface>('Recipes', recipeSchema);

export default Recipes;