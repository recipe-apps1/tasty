import { NextFunction, Request, Response } from 'express';
import User from '../models/users';
import { UserDocInterface } from '../types/types';

export default function (req: Request & { authToken?: string, user?: UserDocInterface }, res: Response, next: NextFunction) {
    const authToken = req.cookies.authToken;
    if (!authToken) {
        return res.status(401).json({ status: 401, success: false, message: 'User not authorized' });
    }
    User.findByToken(authToken, (err: any, user: UserDocInterface) => {
        if (err) {
            return res.status(401)
                .cookie("authToken", "", { httpOnly: true, secure: true })
                .json({ status: 401, success: false, message: 'User not authorized' });
        } else if (!user) {
            return res.status(404)
                .cookie("authToken", "", { httpOnly: true, secure: true })
                .json({ status: 404, success: false, message: 'User not found' });
        }
        req.authToken = authToken;
        req.user = user;
        next();
    });
};