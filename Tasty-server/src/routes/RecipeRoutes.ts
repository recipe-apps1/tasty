import express from "express";
import {
  CreateRecipe,
  GetFavouriteRecipes,
  GetMyRecipes,
  GetRecipe,
} from "../controllers/RecipeController";
import Auth from "../middleware/Auth";
import multer from "multer";
import {
  FavouriteRecipe,
  UnfavouriteRecipe,
} from "../controllers/UserController";

const multerMid = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024, // 5 MB
  },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
    }
  },
});

const router = express.Router();
router.post("/create", Auth, multerMid.single("image"), CreateRecipe);
router.get("/get/my", Auth, GetMyRecipes);
router.get("/get/favourites", Auth, GetFavouriteRecipes);
router.get("/get/:id", GetRecipe);
router.post("/favourite", Auth, FavouriteRecipe);
router.put("/unfavourite", Auth, UnfavouriteRecipe);

export default router;
