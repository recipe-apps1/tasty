import express from "express";
import { CreateUser, LoginUser, Authenticate, LogoutUser } from "../controllers/UserController";
import Auth from "../middleware/Auth";

const router = express.Router();
router.post("/create", CreateUser);
router.post("/login", LoginUser);
router.post("/logout", Auth, LogoutUser);
router.get("/authenticate", Auth, Authenticate);

export default router;
