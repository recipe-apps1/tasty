import { Model, Document, ObjectId } from 'mongoose';

export interface UserDocInterface extends Document {
    _id: ObjectId,
    email: string,
    password: string,
    recipeList: any[],
    name: string,
    myRecipes: any[],
    authToken?: string,
    generateToken: (callback: Function) => void
    comparePassword: (password: string, callback: Function) => void,
    findByToken: (token: string, callBack: Function) => void
};

export interface User {
    _id: ObjectId,
    email: string,
    password: string,
    recipeList?: any[],
    name: string,
    myRecipes?: any[],
    authToken?: string
};

export interface UserLogin {
    email: string,
    password: string
}

export interface UserModelInterface extends Model<UserDocInterface> {
    save(user: User): UserDocInterface;
    generateToken: (callback: Function) => void
    comparePassword: (password: string, callback: Function) => void,
    findByToken: (authToken: string, callBack: Function) => void
};

export interface RecipeModelInterface extends Document {
    _id: ObjectId,
    title: string,
    tags: string[],
    ingredients: string[],
    instructionLink?: string,
    instructions?: string,
    image: string,
    createdBy: ObjectId
};

export interface Recipe {
    _id: ObjectId,
    title: string,
    tags: string[],
    ingredients: string[],
    instructionLink?: string,
    instructions?: string,
    image: string,
    createdBy: ObjectId | undefined
};