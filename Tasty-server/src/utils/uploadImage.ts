import util from "util";
import { createHmac } from 'crypto';
import GoogleCloud from "../services/GoogleStorage";
import { logger } from "../loggers/Logger";
const bucket = GoogleCloud.bucket(process.env.GOOGLE_BUCKET || "");

const SECRET = "super-tasty-images";
const CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

const getFolderKey = (string: string): string => {
  try {
    const key = createHmac('sha256', SECRET).update(string).digest('hex');
    return key;
  } catch (error: any) {
    console.log(error);
    return string;
  }
}

const generateString = (length: number) => {
  let result = "";
  for (let i = 0; i < length; i++) {
    result += CHARACTERS.charAt(Math.floor(Math.random() * CHARACTERS.length));
  }
  return result;
};

export const uploadImage = (file: any, userEmail: string): Promise<string> => {
  logger.info(`Uploading image from user: ${userEmail}`);
  return new Promise(async (resolve, reject) => {
    const { buffer, originalname } = file;
    const folderKey = await getFolderKey(userEmail);
    const path = `${folderKey}/${generateString(16)}.${ originalname.split(".")[1]}`;
    const blob = bucket.file(path);
    const blobStream = blob.createWriteStream({ resumable: false });
    blobStream
      .on("finish", () => {
        const publicUrl = util.format(`https://storage.googleapis.com/${bucket.name}/${path}`);
        resolve(publicUrl);
      })
      .on("error", reject)
      .end(buffer);
  });
};
