import { User, UserLogin } from "../types/types";
import Joi from "joi";

export const validateRegistration = (data: User) => {
  const scheme = Joi.object({
    email: Joi.string().min(3).max(320).required().email({ minDomainSegments: 2, tlds: { allow: false } }),
    password: Joi.string().min(6).max(100).required().pattern(new RegExp("[a-zA-Z0-9]")),
    name: Joi.string().min(2).max(100).required().pattern(new RegExp("[a-zA-Z0-9_ ]")),
  });
  return scheme.validate(data);
};

export const validateLogin = (data: UserLogin) => {
  const scheme = Joi.object({
    email: Joi.string().min(3).max(320).required().email({ minDomainSegments: 2, tlds: { allow: false } }),
    password: Joi.string().min(6).max(100).required().pattern(new RegExp("[a-zA-Z0-9]")),
  });
  return scheme.validate(data);
};

export const validateRecipe = (data: any) => {
  const scheme = Joi.object({
    title: Joi.string().min(3).max(100).required().pattern(new RegExp("[a-zA-Z0-9_ .\-\/]")),
    tags: Joi.array().items(Joi.string().min(3).max(100).pattern(new RegExp("[a-zA-Z0-9_-]"))).required(),
    ingredients: Joi.array().items(Joi.string().min(3).max(100).pattern(new RegExp("[a-zA-Z0-9_ .\-\/]"))).required(),
    instructionLink: Joi.string().uri({ allowRelative: true }),
    instructions: Joi.string().min(20).pattern(new RegExp("[a-zA-Z0-9_ .\-\/]")),
  });
  return scheme.validate(data);
};

export const validateId = (id: string) => {
  const scheme = Joi.object({
    id: Joi.string().hex().length(24)
  });
  return scheme.validate({ id });
}
