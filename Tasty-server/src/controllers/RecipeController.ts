import { Request, Response } from "express";
import { logger } from "../loggers/Logger";
import Recipes from "../models/recipes";
import { saveRecipe } from "../services/RecipeService";
import { UserDocInterface, Recipe } from "../types/types";
import { validateId, validateRecipe } from "../utils/validator";

export const CreateRecipe = (
  req: Request & { user?: UserDocInterface },
  res: Response
) => {
    // Form data returns arrays as a string so we need to need to modify it to be an array
    req.body.tags = req.body.tags.split(",");
    req.body.ingredients = req.body.ingredients.split(",");
    const { error } = validateRecipe(req.body);
    if (error) {
        logger.error(`Status: 400. Invalid recipe data: ${error.details[0].message}`);
        return res.status(400).json({ 
            success: false, 
            message: error.details[0].message, 
            status: 400 
        });
    }

    if (req.user) {
        saveRecipe(req as Request & { user: UserDocInterface }, res);
    }
};

export const GetMyRecipes = (
    req: Request & { user?: UserDocInterface },
    res: Response
) => {
    if (req.user) {
        Recipes.find(
            { _id : { $in : req.user.myRecipes } },
            (err: any, recipes: Recipe[]) => {
                if (err) {
                    logger.error(`Status: 500. Something went wrong: ${err.details[0].message}`);
                    return res.status(500).json({
                        status: 500,
                        success: false,
                        message: "Something went wrong",
                    });
                } else {
                    logger.info(`Status 200. Successfully retreived user's recipes: ${recipes}`);
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: "Successfully retreived user's recipes.",
                        recipes
                    });
                }
            }
        )
    }
};

export const GetRecipe = (req: Request, res: Response) => {
    const { error } = validateId(req.params.id);

    if (error) {
        logger.error(`Status: 400. Invalid url: ${error.details[0].message}`);
        return res.status(400).json({ 
            success: false, 
            message: error.details[0].message, 
            status: 400 
        });
    }

    Recipes.findOne(
        { _id : req.params.id },
        (err: any, recipe: Recipe) => {
            if (err) {
                logger.error(`Status: 500. Something went wrong: ${err.details[0].message}`);
                return res.status(500).json({
                    status: 500,
                    success: false,
                    message: "Something went wrong",
                });
            } else if (recipe) {
                logger.info(`Status 200. Successfully retreived a recipe: ${recipe}`);
                return res.status(200).json({
                    status: 200,
                    success: true,
                    message: "Successfully retreived a recipe.",
                    recipe
                });
            } else {
                logger.info(`Status 404. No recipe found.`);
                return res.status(404).json({
                    status: 404,
                    success: false,
                    message: "No recipe found.",
                });
            }
        }
    )
};

export const GetFavouriteRecipes = (
    req: Request & { user?: UserDocInterface },
    res: Response
) => {
    if (req.user) {
        Recipes.find(
            { _id : { $in : req.user.recipeList } },
            (err: any, recipes: Recipe[]) => {
                if (err) {
                    logger.error(`Status: 500. Something went wrong: ${err.details[0].message}`);
                    return res.status(500).json({
                        status: 500,
                        success: false,
                        message: "Something went wrong",
                    });
                } else {
                    logger.info(`Status 200. Successfully retreived user's favourite recipes: ${recipes}`);
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: "Successfully retreived user's favourite recipes.",
                        recipes
                    });
                }
            }
        )
    }
};
