import { Request, Response } from "express";
import { logger } from "../loggers/Logger";
import User from "../models/users";
import { User as UserType, UserDocInterface } from "../types/types";
import { validateLogin, validateRegistration } from "../utils/validator";

export const CreateUser = (req: Request, res: Response) => {
  const { error } = validateRegistration(req.body);
  if (error) {
    logger.error(`Status: 400. Invalid data: ${error.details[0].message}`);
    return res
      .status(400)
      .json({ success: false, message: error.details[0].message, status: 400 });
  }
  const user = new User(req.body);
  user.save((err) => {
    if (err) {
      if (err.message.includes("duplicate key error")) {
        logger.error(`Status: 409. Email already exists: ${err.message}`);
        return res.status(409).json({
          status: 409,
          success: false,
          message: "Email already exists",
        });
      }
      logger.error(`Status: 422. Internal server error: ${err.message}`);
      return res.status(422).json({
        status: 422,
        success: false,
        message: "Internal server error",
        errors: err,
      });
    } else {
      // generate token after successful registration for user to be automatically logged in
      user.generateToken((err: any, userData: UserType) => {
        handleTokenGeneration(res, err, userData, "Successfully signed up");
      });
    }
  });
};

export const LoginUser = (req: Request, res: Response) => {
  const { error } = validateLogin(req.body);
  if (error) {
    return res
      .status(400)
      .json({ success: false, message: error.details[0].message, status: 400 });
  }
  User.findOne(
    { email: req.body.email },
    (err: any, user: UserDocInterface) => {
      if (!user) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: "User email not found",
        });
      } else {
        user.comparePassword(
          req.body.password,
          (err: any, isMatch: Boolean) => {
            if (!isMatch) {
              return res.status(400).json({
                status: 400,
                success: false,
                message: "Wrong Password",
              });
            } else {
              user.generateToken((err: any, userData: UserType) => {
                handleTokenGeneration(
                  res,
                  err,
                  userData,
                  "Successfully logged in"
                );
              });
            }
          }
        );
      }
    }
  );
};

const handleTokenGeneration = (
  res: Response,
  err: any,
  user: UserType,
  successMessage: string
) => {
  if (err) {
    logger.error(
      `Status: 400. Failed to generate token: ${JSON.stringify(err)}`
    );
    return res.status(400).send({
      success: false,
      message: "Failed to generate token",
      errors: err,
      status: 400,
    });
  } else {
    const data = {
      _id: user._id,
      name: user.name,
      email: user.email,
      recipeList: user?.recipeList,
      myRecipes: user?.myRecipes,
    };
    logger.info(`Status: 200. ${successMessage}`);
    res
      .cookie("authToken", user.authToken, { httpOnly: true, secure: true })
      .status(200)
      .json({
        status: 200,
        success: true,
        message: successMessage,
        user: data,
      });
  }
};

export const Authenticate = (
  req: Request & { user?: UserDocInterface },
  res: Response
) => {
  if (req.user) {
    logger.info(
      `Status: 200. Successfully authenticated user: ${req.user.email}`
    );
    res.status(200).json({
      status: 200,
      success: true,
      message: "Successfully authenticated",
      user: {
        _id: req.user._id,
        name: req.user.name,
        email: req.user.email,
        recipeList: req.user?.recipeList,
        myRecipes: req.user?.myRecipes,
      },
    });
  }
};

export const LogoutUser = (
  req: Request & { user?: UserDocInterface },
  res: Response
) => {
  if (req.user) {
    logger.info(`Status: 200. Successfully logged out user: ${req.user.email}`);
  }
  res
    .cookie("authToken", "", { httpOnly: true, secure: true })
    .status(200)
    .json({
      status: 200,
      success: true,
      message: "Successfully logged out.",
    });
};

export const FavouriteRecipe = (
  req: Request & { user?: UserDocInterface },
  res: Response
) => {
  const { recipeId } = req.body;
  const { user } = req;
  if (user) {
    if (!user?.recipeList.includes(recipeId)) {
      User.updateOne(
        { _id: user._id },
        { $push: { recipeList: recipeId } },
        (err: any) => {
          if (err) {
            logger.error(
              `Status: 400. Failed to update user info: ${err.details[0].message}`
            );
            return res.status(400).json({
              success: false,
              message: err.details[0].message,
              status: 400,
            });
          } else {
            logger.info(
              `Status: 200. Successfully favourited recipe: ${recipeId} for user: ${user.email}`
            );
            console.log(user);
            return res.status(200).json({
              success: true,
              message: "Successfully favourited a recipe.",
              status: 200,
              user,
            });
          }
        }
      );
    }
  } else {
    logger.error(`Status: 401. No user found favouriting recipe: ${recipeId}`);
    return res.status(401).json({
      success: false,
      message: "No user found.",
      status: 401,
    });
  }
};

export const UnfavouriteRecipe = (
  req: Request & { user?: UserDocInterface },
  res: Response
) => {
  const { recipeId } = req.body;
  const { user } = req;
  if (user) {
    if (user?.recipeList.includes(recipeId)) {
      User.updateOne(
        { _id: user._id, recipeList: recipeId },
        { $pull: { recipeList: recipeId } },
        (err: any) => {
          if (err) {
            logger.error(
              `Status: 400. Failed to update user info: ${err.details[0].message}`
            );
            return res.status(400).json({
              success: false,
              message: err.details[0].message,
              status: 400,
            });
          } else {
            logger.info(
              `Status: 200. Successfully unfavourited recipe: ${recipeId} for user: ${user.email}`
            );
            console.log(user);
            return res.status(200).json({
              success: true,
              message: "Successfully unfavourited a recipe.",
              status: 200,
              user,
            });
          }
        }
      );
    }
  } else {
    logger.error(
      `Status: 401. No user found unfavouriting recipe: ${recipeId}`
    );
    return res.status(401).json({
      success: false,
      message: "No user found.",
      status: 401,
    });
  }
};
