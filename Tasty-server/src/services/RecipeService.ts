import { Request, Response } from "express";
import { logger } from "../loggers/Logger";
import Recipes from "../models/recipes";
import Users from "../models/users";
import { Recipe, UserDocInterface } from "../types/types";
import { uploadImage } from "../utils/uploadImage";

export const saveRecipe = async (req: Request & { user: UserDocInterface }, res: Response): Promise<void> => {
    try {
        const image: string = await uploadImage(req.file, req.user.email);
        const newRecipe: any = {
            createdBy: req.user._id,
            title: req.body.title,
            tags: req.body.tags,
            ingredients: req.body.ingredients,
            image,
            instructionLink: req.body.instructionLink,
            instructions: req.body.instructions
        };

        logger.info(`Creating new recipe: ${JSON.stringify(newRecipe)}`);
        const recipe: Recipe = await new Recipes(newRecipe).save();
        await Users.updateOne(
            { _id: req.user?._id },
            { $push: { myRecipes: recipe._id } }
        );
        res.status(200).json({
            status: 200,
            success: true,
            message: "Successfully created a new recipe",
            recipe: recipe,
        });
    } catch(error: any) {
        logger.error(`Status: 500. Recipe service: Something went wrong: ${JSON.stringify(error)}`);
        res.status(500).json(error);
    }
}