import { Storage } from '@google-cloud/storage';

const GoogleStorage = new Storage();

export default GoogleStorage;