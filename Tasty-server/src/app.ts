import express from 'express';
import { loggerMiddleware } from "./loggers/Logger";
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import { normalizePort } from './utils/normalizePort';
import userRouter from './routes/UserRoutes';
import recipeRouter from "./routes/RecipeRoutes";

require('dotenv').config();
const app = express();

const port = normalizePort(process.env.PORT || '3001');

app.use(loggerMiddleware);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors({
    origin: ["http://localhost:3000"],
    credentials: true
}));

// Route middleware
app.use('/api/users', userRouter);
app.use('/api/recipes', recipeRouter);

// Connection to MongoDB
const uri = process.env.MONGO_URI || "";
mongoose.connect(uri, (err) => {
    if (err) {
        console.log(err);
        throw err;
    } else {
        console.log("Mongo connected.");
    }
});

/**
    * Create HTTP server.
*/
app.listen(port, () => {
    console.log(`Server created. Listening on port ${port}`);
});
 