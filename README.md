# Getting Started with Tasty-app

Before getting started, make sure the node_modules are installed in the Tasty-app directory with `npm install`

## Available Scripts

In the project directory Tasty-app, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `dist` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

# Getting Started with Tasty-server

Before getting started, make sure the node_modules are installed in the Tasty-server directory with `npm install`

You will need to create a ".env" file in the Tasty-server directory and store sensitive credentials there. There is a ".envExample" file to serve as an example how your .env file should look like.

## Available Scripts

In the project directory Tasty-server, you can run:

### `npm start`

Runs the server in the development mode.\
Open [http://localhost:3001](http://localhost:3001) to view it in your browser.

The server will reload when you make changes.

# Git lifecycle

## Getting the latest changes

To retrieve the latest changes for the project you can write `git pull` in the "main" branch, if you want to pull the latest changes in a certain branch, you can write `git pull origin your-branch-name`.

## Pushing your changes to git

First thing you need to do is create a new branch (branch out) from the main branch, your branches name should be like this: "TAS-<your-task-number>-details-about-the-task". You can do it with `git checkout -b your-branch-name`.

To push the changes to git, you need to add the changes you want to push to git e.g. `git add .` to add all the changes you made in your branch. If you want to add a certain file, you can write `git add ./path/to/certain/file`.

When you added the files you want to push, you need to commit the changes and give it some details about the changes you made by writing `git commit -m "Some details about my task"`.

After the changes have been commited you can push the changes to your branch with `git push origin your-branch-name`.

