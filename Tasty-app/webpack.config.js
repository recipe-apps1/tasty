const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = async (env, argv) => {
    const isDev = argv.mode === 'development';
    const plugins = [];

    return ({
        entry: './index.js',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'index_bundle.js',
            publicPath: '/',
        },
        mode: isDev ? 'development' : 'production',
        devtool: isDev ? 'source-map' : false,
        // webpack 5 comes with devServer which loads in development mode
        devServer: {
            port: 3000,
            client: {
                logging: 'info',
                progress: true
            },
            hot: true,
            compress: true,
            historyApiFallback: true
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.(png|jpe?g|gif|xlsx|ttf|otf|svg)$/i,
                    loader: 'file-loader',
                    options: {
                        name(resourcePath, resourceQuery) {
                            // `resourcePath` - `/absolute/path/to/file.js`
                            // `resourceQuery` - `?foo=bar`

                            if (isDev) {
                                return '[path][name].[ext]';
                            }
                            return '[contenthash].[ext]';
                        },
                    },
                }
            ]
        },
        plugins: [
            ...plugins,
            new HtmlWebpackPlugin({
                template: './index.html'
            })
        ]
    });
};