import React, { useEffect, useState } from "react";
import Header from "./components/Header/Header.js";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import SignInPage from "./pages/SignIn/SignInPage.js";
import SignUpPage from "./pages/Signup/SignUpPage.js";
import Homepage from "./pages/Homepage/Homepage.js";
import { LoginContext } from "./Helper/Context.js";
import axios from "axios";
import RecipePage from "./pages/RecipePage/RecipePage.js";
import MyRecipePage from "./pages/MyRecipePage/MyRecipePage.js";
import RecipeIDPage from "./pages/RecipeIDPage/RecipeIDPage.js";
import LikedRecipesPage from "./pages/LikedRecipesPage/LikedRecipesPage.js";

function App() {
  const [userData, setUserData] = useState({});
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    axios
      .get("http://localhost:3001/api/users/authenticate", {
        withCredentials: true,
      })
      .then(function (response) {
        setUserData(response.data.user);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        setLoaded(true);
      });
  }, []);

  if (loaded) {
    return (
      <LoginContext.Provider value={{ userData, setUserData }}>
        <Router>
          <Header />
          <Routes>
            <Route path="/" element={<Homepage />} />
            {!userData._id ? (
              <>
                <Route path="sign-in" element={<SignInPage />} />
                <Route path="sign-up" element={<SignUpPage />} />
              </>
            ) : (
              <>
                <Route path="create-recipe" element={<RecipePage />} />
                <Route path="my-recipes" element={<MyRecipePage />} />
                <Route path="recipes/:id" element={<RecipeIDPage />} />
                <Route path="liked-recipes" element={<LikedRecipesPage />} />
              </>
            )}
          </Routes>
        </Router>
      </LoginContext.Provider>
    );
  }
  return null;
}

export default App;
