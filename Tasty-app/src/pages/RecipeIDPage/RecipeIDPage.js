import axios from "axios";
import React, { useEffect, useState, useContext } from "react";
import { LoginContext } from "../../Helper/Context.js";
import styles from "./RecipeIDPage.module.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
} from "react-router-dom";
import { useNavigate } from "react-router-dom";
import heart from "../../assets/images/Heart.png";
import crossedHeart from "../../assets/images/CrossedHeart.png";

function RecipeIDPage() {
  const { userData, setUserData } = useContext(LoginContext);
  const [recipe, setRecipe] = useState(null);
  const history = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`http://localhost:3001/api/recipes/get/${id}`, {
        withCredentials: true,
      })
      .then(function (response) {
        setRecipe(response.data.recipe);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const handleFavourite = () => {
    axios
      .post(
        `http://localhost:3001/api/recipes/favourite`,
        { recipeId: id },
        {
          withCredentials: true,
        }
      )
      .then(function () {
        setUserData({ ...userData, recipeList: [...userData.recipeList, id] });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleUnfavourite = () => {
    axios
      .put(
        `http://localhost:3001/api/recipes/unfavourite`,
        { recipeId: id },
        {
          withCredentials: true,
        }
      )
      .then(function () {
        setUserData({
          ...userData,
          recipeList: userData.recipeList.filter((data) => data !== id),
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleClick = (e) => {
    console.log("bruh");
  };

  const handleGoBack = () => {
    history("/my-recipes");
  };

  if (!recipe) return null;
  return (
    <div className={styles.container}>
      <div>
        <div className={styles.title}>{recipe.title}</div>

        <div className={styles.subContainer}>
          <span className={styles.text}>
            Ingredients:{" "}
            <span className={styles.ingredients}>
              {recipe.ingredients.join(", ")}
            </span>
          </span>
        </div>

        <div className={styles.subContainer}>
          <span className={styles.text}>
            Tags: <span className={styles.tags}>{recipe.tags.join(", ")}</span>
          </span>
        </div>

        {recipe.instructions && (
          <div className={styles.subContainer}>
            <span className={styles.text}>
              Instructions:{" "}
              <span className={styles.instructions}>{recipe.instructions}</span>
            </span>
          </div>
        )}

        {recipe.instructionLink && (
          <div className={styles.subContainer}>
            <span className={styles.text}>
              Instruction link:{" "}
              <a className={styles.link} href={recipe.instructionLink}>
                {recipe.instructionLink}
              </a>
            </span>
          </div>
        )}

        <div className={styles.buttonContainer}>
          {userData.recipeList.indexOf(id) === -1 ? (
            <button
              className={styles.FavouriteButton}
              onClick={handleFavourite}
            >
              <img className={styles.FavouriteButtonImg} src={heart}></img>
            </button>
          ) : (
            <button
              className={styles.FavouriteButton}
              onClick={handleUnfavourite}
            >
              <img
                className={styles.FavouriteButtonImg}
                src={crossedHeart}
              ></img>
            </button>
          )}
        </div>

        {userData.myRecipes.indexOf(id) !== -1 ? (
          <div className={styles.buttonContainer}>
            <button
              className={styles.DeleteButton}
              type="button"
              onClick={handleClick}
            >
              Delete
            </button>
            <button
              className={styles.EditButton}
              type="button"
              onClick={handleClick}
            >
              Edit
            </button>
          </div>
        ) : null}

        <div className={styles.GoBackContainer}>
          <button
            className={styles.GoBackButton}
            type="button"
            onClick={handleGoBack}
          >
            Go back
          </button>
        </div>
      </div>

      <div className={styles.imagePosition}>
        <img className={styles.image} src={recipe.image}></img>
      </div>
    </div>
  );
}

export default RecipeIDPage;
