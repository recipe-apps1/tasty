import React, { useState, useContext } from "react";
import styles from "./SignUpPage.module.css";
import axios from "axios";
import { LoginContext } from "../../Helper/Context";
import { useNavigate } from "react-router-dom";
import InputField from "../../components/InputField/InputField";
import Button from "../../components/Button/Button";

const SignUpPage = () => {
  const [formValue, setformValue] = React.useState({
    username: "",
    email: "",
    password: "",
    repeatPassword: "",
  });

  const [error, setError] = React.useState({
    hasError: false,
    message: "",
  });
  const history = useNavigate();

  const { setUserData } = useContext(LoginContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (formValue.password === formValue.repeatPassword) {
      axios
        .post(
          "http://localhost:3001/api/users/create",
          {
            name: formValue.username,
            password: formValue.password,
            email: formValue.email,
          },
          {
            withCredentials: true,
          }
        )
        .then(function (response) {
          console.log(response);
          history("/");
          setUserData(response.data.user);
        })
        .catch(function (error) {
          setError({ hasError: true, message: error.response.data.message });
        });
    } else {
      setError({ hasError: true, message: "All input boxes must be filled" });
    }
  };

  const handleChange = (event) => {
    if (error.hasError) {
      setError({ hasError: false, message: "" });
    }
    setformValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className={styles.background}>
      <form onSubmit={handleSubmit}>
        <InputField
          type="text"
          name="username"
          placeholder="ENTER A USERNAME"
          required
          value={formValue.username}
          onChange={handleChange}
        />
        <InputField
          type="email"
          name="email"
          placeholder="ENTER EMAIL"
          value={formValue.email}
          onChange={handleChange}
        />
        <InputField
          type="password"
          name="password"
          placeholder="ENTER A PASSWORD"
          minLength="6"
          required
          onChange={handleChange}
        />
        <InputField
          type="password"
          name="repeatPassword"
          placeholder="REPEAT PASSWORD"
          minLength="6"
          required
          onChange={handleChange}
        />
        {error.hasError ? <div>{error.message}</div> : null}
        <Button type="submit" text="Register" />
      </form>
    </div>
  );
};
export default SignUpPage;
