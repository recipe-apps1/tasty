import React, { useState, useContext, useEffect } from "react";
import RecipeCard from "../../components/RecipeCard/RecipeCard";
import styles from "./LikedRecipesPage.module.css";
import axios from "axios";

function LikedRecipesPage() {
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3001/api/recipes/get/favourites", {
        withCredentials: true,
      })
      .then(function (response) {
        setRecipes(response.data.recipes);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  if (!recipes) return null;
  return (
    <div className={styles.background}>
      {recipes.map((recipe, index) => (
        <RecipeCard
          key={index}
          title={recipe.title}
          image={recipe.image}
          tags={recipe.tags}
          ingredients={recipe.ingredients}
          _id={recipe._id}
        />
      ))}
    </div>
  );
}

export default LikedRecipesPage;
