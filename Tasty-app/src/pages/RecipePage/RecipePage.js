import React, { useEffect, useState } from "react";
import styles from "./RecipePage.module.css";
import InputField from "../../components/InputField/InputField";
import Button from "../../components/Button/Button";
import axios from "axios";
import Select from "react-select";
import validator from "validator";
import { useNavigate } from "react-router-dom";

const options = [
  { value: "link", label: "Add instruction link" },
  { value: "manual", label: "Add instructions manually" },
];

function RecipePage() {
  const [title, setTitle] = useState("");
  const [tag, setTag] = useState("");
  const [tags, setTags] = useState([]);
  const [ingredient, setIngredient] = useState("");
  const [ingredients, setIngredients] = useState([]);
  const [selectedImage, setSelectedImage] = useState();
  const [selectedOption, setSelectedOption] = useState(options[0]);
  const [urlError, setUrlError] = useState("");
  const [link, setLink] = useState("");
  const [instructions, setInstructions] = useState("");
  const [errorIngredient, setErrorIngredient] = useState("");
  const [errorTag, setErrorTag] = useState("");
  const history = useNavigate();

  const handleTitleChange = (e) => setTitle(e.target.value);
  const onTagChange = (e) => setTag(e.target.value.replace(/[ ,]/g, ""));
  const deleteTag = (index) => setTags(tags.filter((tag, i) => i !== index));
  const onIngredientChange = (e) =>
    setIngredient(e.target.value.replace(/[,]/g, ""));
  const deleteIngredient = (index) =>
    setIngredients(ingredients.filter((ingredient, i) => i !== index));

  const onKeyDownTag = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      addTag();
    }
  };

  const addTag = () => {
    if (tag.length < 3) {
      setErrorTag("Tag must be atleast 3 letters long");
    } else if (!tags.includes(tag)) {
      tags.unshift(tag);
      setTags(tags);
      setTag("");
      setErrorTag("");
    } else {
      setErrorTag("This tag already exists");
    }
  };

  const onKeyDownIngredient = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      addIngredient();
    }
  };

  const addIngredient = () => {
    if (ingredient.length < 3) {
      setErrorIngredient("Ingredient name must be atleast 3 letters long");
    } else if (!ingredients.includes(ingredient)) {
      ingredients.unshift(ingredient);
      setIngredients(ingredients);
      setIngredient("");
      setErrorIngredient("");
    } else {
      setErrorIngredient("This ingredient already exists");
    }
  };

  const onSelectChange = (value) => {
    setSelectedOption(value);
    if (urlError) {
      setUrlError("");
    }
  };

  const validate = (value) => {
    setLink(value);
    if (validator.isURL(value)) {
      setUrlError("");
    } else {
      setUrlError("Is not a valid URL");
    }
  };

  const handleInstructionChange = (event) => {
    setInstructions(event.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("image", selectedImage);
    formData.append("title", title);
    formData.append("tags", tags);
    formData.append("ingredients", ingredients);
    if (selectedOption.value === "link") {
      // data.recipeLink = link;
      formData.append("instructionLink", link);
    } else {
      // data.instructions = instructions;
      formData.append("instructions", instructions);
    }
    axios
      .post(`http://localhost:3001/api/recipes/create`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
        withCredentials: true,
      })
      .then(() => {
        console.log("success");
        history("/");
      })
      .catch((err) => console.log(err));
  };

  const onFileChange = (e) => {
    setSelectedImage(e.target.files[0]);
  };

  const removeFileChange = () => {
    setSelectedImage();
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <InputField
          type="text"
          name="title"
          placeholder="Enter recipe title"
          value={title}
          onChange={handleTitleChange}
          required
        />

        {/* Tags */}
        <InputField
          value={tag}
          placeholder="Enter a tag"
          onChange={onTagChange}
          onKeyDown={onKeyDownTag}
          required
        />

        <Button text="Add tag" onClick={addTag} />

        {tags.map((tag, index) => (
          <div key={index}>
            {tag}
            <button type="button" onClick={() => deleteTag(index)}>
              x
            </button>
          </div>
        ))}

        {errorTag !== "" ? <span>{errorTag}</span> : null}

        {/* Ingredients */}
        <InputField
          value={ingredient}
          placeholder="Enter a ingredient"
          onChange={onIngredientChange}
          onKeyDown={onKeyDownIngredient}
          required
        />
        <Button type="button" text="Add ingredient" onClick={addIngredient} />

        {ingredients.map((ingredient, index) => (
          <div key={index}>
            {ingredient}
            <button type="button" onClick={() => deleteIngredient(index)}>
              x
            </button>
          </div>
        ))}

        {errorIngredient !== "" ? <span>{errorIngredient}</span> : null}

        {/* Selection box */}
        <Select
          value={selectedOption}
          onChange={onSelectChange}
          options={options}
        />
        {selectedOption.value === "link" ? (
          <InputField
            type="text"
            name="link"
            placeholder="Enter a link"
            value={link}
            onChange={(e) => validate(e.target.value)}
            minLength="6"
            required
          />
        ) : null}
        <span>{urlError}</span>
        {selectedOption.value === "manual" ? (
          <textarea
            type="textarea"
            name="manual"
            placeholder="Enter instructions for recipe"
            value={instructions}
            onChange={handleInstructionChange}
            minLength="20"
            required
          />
        ) : null}

        <div className={styles.container}>
          <InputField
            className={styles.customFileInput}
            accept="image/*"
            type="file"
            onChange={onFileChange}
            required
          />

          {selectedImage && (
            <div className={styles.previewImage}>
              <img
                src={URL.createObjectURL(selectedImage)}
                className={styles.imageCurrent}
                alt="Thumb"
              />

              <Button
                onClick={removeFileChange}
                className={styles.deleteImage}
                type="button"
                text="Delete current image"
              />
            </div>
          )}
        </div>
        <Button type="submit" text="Upload" onClick={handleSubmit} />
      </form>
    </div>
  );
}

export default RecipePage;
