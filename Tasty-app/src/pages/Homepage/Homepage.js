import { useState } from "react";
import React from "react";
import styles from "./Homepage-module.css";
import Button from "../../components/Button/Button";
import InputField from "../../components/InputField/InputField";

function Homepage() {
  const [input, setInput] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <div className="Ainsley">
      <form onSubmit={handleSubmit}>
        <InputField
          className="field_input"
          type="text"
          value={input}
          placeholder="Search for a recipe"
          onChange={(e) => setInput(e.target.value)}
        />
        <Button type="submit" text="Search" />
      </form>
    </div>
  );
}

export default Homepage;
