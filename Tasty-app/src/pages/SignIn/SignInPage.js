import React, { useContext } from "react";
import styles from "./SignInPage.module.css";
import axios from "axios";
import { LoginContext } from "../../Helper/Context.js";
import { useNavigate } from "react-router-dom";
import InputField from "../../components/InputField/InputField";
import Button from "../../components/Button/Button";

function SignInPage() {
  const [formValue, setformValue] = React.useState({
    email: "",
    password: "",
  });
  const [error, setError] = React.useState({
    hasError: false,
    message: "",
  });
  const history = useNavigate();

  const { setUserData } = useContext(LoginContext);

  const handleSubmit = (event) => {
    event.preventDefault();

    axios
      .post("http://localhost:3001/api/users/login", formValue, {
        withCredentials: true,
      })
      .then(function (response) {
        history("/");
        setUserData(response.data.user);
      })
      .catch(function (error) {
        setError({ hasError: true, message: error.response.data.message });
      });
  };

  const handleChange = (event) => {
    if (error.hasError) {
      setError({ hasError: false, message: "" });
    }
    setformValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className={styles.background}>
      <form onSubmit={handleSubmit}>
        <InputField
          className={styles.inputLogin}
          type="email"
          name="email"
          placeholder="ENTER AN EMAIL"
          value={formValue.email}
          onChange={handleChange}
        />
        <InputField
          className={styles.inputPassword}
          type="password"
          name="password"
          placeholder="ENTER A PASSWORD"
          value={formValue.password}
          onChange={handleChange}
        />
        {error.hasError ? <div>{error.message}</div> : null}
        <Button type="submit" text="Login" />
      </form>
    </div>
  );
}

export default SignInPage;
