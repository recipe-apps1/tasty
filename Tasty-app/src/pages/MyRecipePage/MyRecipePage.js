import axios from "axios";
import React, { useEffect, useState } from "react";
import styles from "./MyRecipePage.module.css";
import RecipeCard from "../../components/RecipeCard/RecipeCard";

function MyRecipePage() {
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3001/api/recipes/get/my", {
        withCredentials: true,
      })
      .then(function (response) {
        setRecipes(response.data.recipes);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  // console.log(recipes);

  return (
    <div className={styles.background}>
      {recipes.map((recipe, index) => (
        <RecipeCard
          key={index}
          title={recipe.title}
          image={recipe.image}
          tags={recipe.tags}
          ingredients={recipe.ingredients}
          _id={recipe._id}
        />
      ))}
    </div>
  );
}

export default MyRecipePage;
