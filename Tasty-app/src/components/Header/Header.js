import React, { useContext } from "react";
import styles from "./Header.module.css";
import { Link } from "react-router-dom";
import { LoginContext } from "../../Helper/Context.js";
import LogoutButton from "../LogoutButton/LogoutButton";
import RecipePage from "../../pages/RecipePage/RecipePage";

const Header = () => {
  const { userData } = useContext(LoginContext);
  return (
    <header className={styles.header}>
      <h1 className={styles.title}>Tasty!</h1>
      <div className={styles.links}>
        <Link to="/">Homepage</Link>
        {!userData._id ? (
          <>
            <Link to="/sign-in">Login</Link>
            <Link to="/sign-up">Registration</Link>
          </>
        ) : (
          <>
            <Link to="/create-recipe">Create Recipe</Link>
            <Link to="/my-recipes">My recipes</Link>
            <Link to="/liked-recipes">Liked recipes</Link>
            <LogoutButton />
          </>
        )}
      </div>
    </header>
  );
};
export default Header;
