import classNames from "classnames";
import React from "react";
import styles from "./Button.module.css";

const Button = ({ className, onClick = () => { }, text, type = "button" }) => {
  return (
    <div className={styles.container}>
      <button
        className={classNames(styles.button, className)}
        onClick={onClick}
        type={type}
      >
        {text}
      </button>
    </div>
  );
};

export default Button;
