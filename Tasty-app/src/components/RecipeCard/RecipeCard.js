import React from "react";
import styles from "./RecipeCard.module.css";
import { Link } from "react-router-dom";
import heart from "../../assets/images/Heart.png";
import crossedHeart from "../../assets/images/CrossedHeart.png";
import { useContext } from "react";
import { LoginContext } from "../../Helper/Context.js";
import axios from "axios";

function RecipeCard({ title, image, tags, ingredients, _id }) {
  const { userData, setUserData } = useContext(LoginContext);

  const handleFavourite = () => {
    axios
      .post(
        `http://localhost:3001/api/recipes/favourite`,
        { recipeId: _id },
        {
          withCredentials: true,
        }
      )
      .then(function () {
        setUserData({ ...userData, recipeList: [...userData.recipeList, _id] });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleUnfavourite = () => {
    axios
      .put(
        `http://localhost:3001/api/recipes/unfavourite`,
        { recipeId: _id },
        {
          withCredentials: true,
        }
      )
      .then(function () {
        setUserData({
          ...userData,
          recipeList: userData.recipeList.filter((data) => data !== _id),
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className={styles.box}>
      <img src={image} className={styles.image}></img>
      <div className={styles.textBox}>
        <div className={styles.title}>{title}</div>
        <div className={styles.detailsBox}>
          <div className={styles.text}>
            Tags: <span className={styles.tags}>{tags.join(", ")}</span>
          </div>
          <div className={styles.text}>
            Ingredients:{" "}
            <span className={styles.ingredients}>{ingredients.join(", ")}</span>
          </div>
        </div>
        <div className={styles.buttonContainer}>
          <div className={styles.FavouritePadding}>
            {userData.recipeList.indexOf(_id) === -1 ? (
              <button className={styles.Button} onClick={handleFavourite}>
                <img className={styles.ButtonImg} src={heart}></img>
              </button>
            ) : (
              <button className={styles.Button} onClick={handleUnfavourite}>
                <img className={styles.ButtonImg} src={crossedHeart}></img>
              </button>
            )}
          </div>
          <div className={styles.linkContainer}>
            <Link to={`/recipes/${_id}`} className={styles.link}>
              Learn more
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RecipeCard;
