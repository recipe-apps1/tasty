import React, { useContext } from "react";
import styles from "./LogoutButton.module.css";
import axios from "axios";
import { LoginContext } from "../../Helper/Context";
import { useNavigate } from "react-router-dom";

const LogoutButton = () => {
  const { setUserData } = useContext(LoginContext);
  const history = useNavigate();
  const logout = function () {
    axios
      .post(
        "http://localhost:3001/api/users/logout",
        {},
        { withCredentials: true }
      )
      .then(function () {
        setUserData({});
        history("/");
      });
  };

  return (
    <button className={styles.logout} onClick={logout}>
      Log out
    </button>
  );
};

export default LogoutButton;
