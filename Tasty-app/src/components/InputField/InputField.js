import React from "react";
import styles from "./InputField.module.css";
import classNames from "classnames";

function InputField(props) {
  return (
    <div className={styles.field_container}>
      <input
        {...props}
        className={classNames(styles.field_input, props.className)}
      />
    </div>
  );
}

export default InputField;
